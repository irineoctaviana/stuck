package com.example.moviesubmission5.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.moviesubmission5.db.WatchHelper;
import com.example.moviesubmission5.ui.favoritelist.ListMovieFragment;
import com.example.moviesubmission5.ui.favoritelist.ListTvShowFragment;
import com.example.moviesubmission5.viewmodel.MainViewModel;
import com.example.moviesubmission5.R;
import com.example.moviesubmission5.entity.Watch;
import com.example.moviesubmission5.adapter.WatchAdapterDetail;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener{
    private WatchAdapterDetail adapter;
    private ProgressBar progressBarDetail;
    private Watch watch;
    private WatchHelper watchHelper;
    private String isMovie;
    private boolean isAlreadyLoaded;
    private Drawable draw;

    public static final String EXTRA_WATCH = "extra_watch";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        RecyclerView recyclerView;
        MainViewModel mainViewModel;
        watchHelper = WatchHelper.getInstance(getApplicationContext());
        watchHelper.open();
        watch = getIntent().getParcelableExtra(EXTRA_WATCH);
        isMovie = watch.getIsMovie();

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        if (isMovie.equals("1")){
            mainViewModel.getDetailWatchMovie().observe(this, getDetailWatch);
        } else {
            mainViewModel.getDetailWatchTvShow().observe(this, getDetailWatch);
        }
        mainViewModel.getErrorMessageDetail().observe(this, getErrorMessageDetail);
        recyclerView = findViewById(R.id.recyclerView);
        progressBarDetail = findViewById(R.id.progressBarDetail);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new WatchAdapterDetail(this);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (isMovie.equals("1")){
            mainViewModel.setDetailWatchMovie(watch.getId());
        } else{
            mainViewModel.setDetailWatchTvShow(watch.getId());
        }
        isAlreadyLoaded = false;
        showLoadingDetail(true);
    }

    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.favorite_action){
            if (!isAlreadyLoaded) {
                Toast.makeText(DetailActivity.this, R.string.patient_message, Toast.LENGTH_SHORT).show();
            } else {
                if (watch != null) {
                    if (checkItemFavorite() == 1) {
                        addItemFavorite();
                        draw = getResources().getDrawable(R.drawable.ic_favorite_full);
                        item.setIcon(draw);
                    } else {
                        showAlertDialog();
                        draw = getResources().getDrawable(R.drawable.ic_favorite_border);
                        item.setIcon(draw);
                    }
                } else {
                    Toast.makeText(DetailActivity.this, R.string.null_message, Toast.LENGTH_SHORT).show();
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.favorite_action);
        if (watch != null) {
            if (checkItemFavorite() == 1) {
                draw = getResources().getDrawable(R.drawable.ic_favorite_border);
                item.setIcon(draw);
            } else {
                draw = getResources().getDrawable(R.drawable.ic_favorite_full);
                item.setIcon(draw);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        watchHelper.close();
    }

    private Observer<ArrayList<Watch>> getDetailWatch = new Observer<ArrayList<Watch>>() {
        @Override
        public void onChanged(ArrayList<Watch> watching) {
            if (watching != null){
                adapter.setDetailWatchAdapter(watching);
                showLoadingDetail(false);
                isAlreadyLoaded = true;
                invalidateOptionsMenu();
            } else {
                showLoadingDetail(false);
            }
        }
    };

    private Observer<String> getErrorMessageDetail = new Observer<String>() {
        @Override
        public void onChanged(String messages) {
            Toast.makeText(DetailActivity.this, messages, Toast.LENGTH_SHORT).show();
        }
    };

    private void showLoadingDetail(Boolean state) {
        if (progressBarDetail != null) {
            if (state) {
                progressBarDetail.setVisibility(View.VISIBLE);
            } else {
                progressBarDetail.setVisibility(View.GONE);
            }
        }
    }

    private void addItemFavorite(){
        long result;
        if (isMovie.equals("1")){
            result = watchHelper.insertMovie(watch);
        } else{
            result = watchHelper.insertTvShow(watch);
        }
        if (result>0){
            Toast.makeText(this, R.string.success_add_message, Toast.LENGTH_SHORT).show();
        } else{
            Toast.makeText(this, R.string.error_add_message, Toast.LENGTH_SHORT).show();
        }
    }

    private void removeItemFavorite(){
        int result;
        if (isMovie.equals("1")){
            result = watchHelper.deleteMovie(watch.getId());
        } else{
            result = watchHelper.deleteTvShow(watch.getId());
        }
        if (result>0){
            Toast.makeText(this, R.string.success_delete_message, Toast.LENGTH_SHORT).show();
        } else{
            Toast.makeText(this, "error_delete_message", Toast.LENGTH_SHORT).show();
        }
    }

    private int checkItemFavorite(){
        boolean result;
        if (isMovie.equals("1")){
            result = watchHelper.checkRowMovie(watch.getId());
        } else{
            result = watchHelper.checkRowTvShow(watch.getId());
        }
        if (result){
            return 1;
        } else{
            return 0;
        }
    }

    private void showAlertDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.title_dialog);
        alertDialogBuilder.setMessage(R.string.message_dialog)
                .setCancelable(false)
                .setPositiveButton(R.string.yes_dialog, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeItemFavorite();
                        finish();
                    }
                }).setNegativeButton(R.string.no_dialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        invalidateOptionsMenu();
    }

    @Override
    public void onBackStackChanged() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.rv_movie_favorite);
        Fragment fragment1 = getSupportFragmentManager().findFragmentById(R.id.rv_tv_show_favorite);
        if (fragment instanceof ListMovieFragment){
            fragment.onResume();
        } else if (fragment1 instanceof ListTvShowFragment){
            fragment1.onResume();
        }
    }
}
