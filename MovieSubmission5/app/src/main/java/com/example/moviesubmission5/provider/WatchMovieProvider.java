package com.example.moviesubmission5.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.example.moviesubmission5.db.DatabaseContract;
import com.example.moviesubmission5.db.WatchHelper;
import com.example.moviesubmission5.ui.favoritelist.ListMovieFragment;

public class WatchMovieProvider extends ContentProvider {
    private static final int WATCH = 1;
    private static final int WATCH_ID = 2;
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private WatchHelper watchHelper;

    static{
        sUriMatcher.addURI(DatabaseContract.AUTHORITY, DatabaseContract.TABLE_MOVIE, WATCH);
        sUriMatcher.addURI(DatabaseContract.AUTHORITY, DatabaseContract.TABLE_MOVIE +"/#", WATCH_ID);
    }

    @Override
    public boolean onCreate() {
        watchHelper = WatchHelper.getInstance(getContext());
        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        watchHelper.open();
        Cursor cursorMovie;
        switch (sUriMatcher.match(uri)){
            case WATCH:
                cursorMovie = watchHelper.queryMovieProvider();
                break;
            case WATCH_ID:
                cursorMovie = watchHelper.queryByIdMovieProvider(uri.getLastPathSegment());
                break;
            default:
                cursorMovie = null;
                break;
        }
        return cursorMovie;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
        watchHelper.open();
        long added;
        switch (sUriMatcher.match(uri)){
            case WATCH:
                added = watchHelper.insertMovieProvider(contentValues);
                break;
            default:
                added = 0;
                break;
        }
        getContext().getContentResolver().notifyChange(DatabaseContract.CONTENT_MOVIE_URI, new ListMovieFragment.DataObserver(new Handler(), getContext()));
        return Uri.parse(DatabaseContract.CONTENT_MOVIE_URI + "/" +added);
    }

    @Override
    public int delete(@NonNull Uri uri, String s, String[] strings) {
        watchHelper.open();
        int deleted;
        switch (sUriMatcher.match(uri)){
            case WATCH_ID:
                deleted = watchHelper.deleteMovieProvider(uri.getLastPathSegment());
                break;
            default:
                deleted = 0;
                break;
        }
        getContext().getContentResolver().notifyChange(DatabaseContract.CONTENT_MOVIE_URI, new ListMovieFragment.DataObserver(new Handler(), getContext()));
        return deleted;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues contentValues, String s, String[] strings) {
        watchHelper.open();
        int updated;
        switch (sUriMatcher.match(uri)){
            case WATCH_ID:
                updated = watchHelper.updateMovieProvider(uri.getLastPathSegment(), contentValues);
                break;
            default:
                updated = 0;
                break;
        }
        getContext().getContentResolver().notifyChange(DatabaseContract.CONTENT_MOVIE_URI, new ListMovieFragment.DataObserver(new Handler(), getContext()));
        return updated;
    }
}
