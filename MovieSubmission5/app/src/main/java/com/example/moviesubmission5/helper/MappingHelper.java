package com.example.moviesubmission5.helper;

import android.database.Cursor;
import android.util.Log;

import com.example.moviesubmission5.db.DatabaseContract.WatchColumns;
import com.example.moviesubmission5.entity.Watch;

import java.util.ArrayList;

public class MappingHelper {
    public static ArrayList<Watch> mapCursorToArrayList(Cursor watchCursor){
        ArrayList<Watch> watchList = new ArrayList<>();
        if (watchCursor == null)
        {
            Log.d("irin", "MappingHelper::watchCursor is NULL");
        }
        else
        {
            Log.d("irin", "MappingHelper::watchCursor is NOT NULL");
        }
        try{
            while (watchCursor.moveToNext()){
                String id = watchCursor.getString(watchCursor.getColumnIndexOrThrow(WatchColumns.WATCH_ID));
                String isMovie = watchCursor.getString(watchCursor.getColumnIndexOrThrow(WatchColumns.WATCH_IS_MOVIE));
                String photoFavorite = watchCursor.getString(watchCursor.getColumnIndexOrThrow(WatchColumns.WATCH_PHOTO));
                String titleFavorite = watchCursor.getString(watchCursor.getColumnIndexOrThrow(WatchColumns.WATCH_TITLE));
                String scoreFavorite = watchCursor.getString(watchCursor.getColumnIndexOrThrow(WatchColumns.WATCH_SCORE));
                String synopsisFavorite = watchCursor.getString(watchCursor.getColumnIndexOrThrow(WatchColumns.WATCH_SYNOPSIS));
                watchList.add(new Watch(id, isMovie, photoFavorite, titleFavorite, scoreFavorite, synopsisFavorite));
            }
        } catch (Throwable e){
            Log.d("irin", "MappingHelper::error " +e.getMessage());
        }
        return watchList;
    }
}
