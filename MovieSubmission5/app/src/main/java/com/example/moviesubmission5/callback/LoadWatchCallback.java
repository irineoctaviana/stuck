package com.example.moviesubmission5.callback;

import android.database.Cursor;

public interface LoadWatchCallback {
    void preExecute();
    void postExecute(Cursor watches);
}
