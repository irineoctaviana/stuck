package com.example.moviesubmission5.db;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
    public static final String AUTHORITY = "com.example.moviesubmission5";
    private static final String SCHEME = "content";
    public static String TABLE_MOVIE = "watchMovie";
    public static String TABLE_TV_SHOW = "watchTvShow";

    public DatabaseContract() { }

    public static final class WatchColumns implements BaseColumns {
        public static String WATCH_ID = "watch_id";
        public static String WATCH_IS_MOVIE = "watch_is_movie";
        public static String WATCH_PHOTO = "watch_photo";
        public static String WATCH_TITLE = "watch_title";
        public static String WATCH_SCORE = "watch_score";
        public static String WATCH_SYNOPSIS = "watch_synopsis";
    }

    public static final Uri CONTENT_MOVIE_URI = new Uri.Builder()
            .scheme(SCHEME)
            .authority(AUTHORITY)
            .appendPath(TABLE_MOVIE)
            .build();

    public static final Uri CONTENT_TV_SHOW_URI = new Uri.Builder()
            .scheme(SCHEME)
            .authority(AUTHORITY)
            .appendPath(TABLE_TV_SHOW)
            .build();

    public static String getColumnString(Cursor cursor, String columnName){
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public static String getColumnInt(Cursor cursor, String columnName){
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public static long getColumnLong(Cursor cursor, String columnName){
        return cursor.getLong(cursor.getColumnIndex(columnName));
    }
}
