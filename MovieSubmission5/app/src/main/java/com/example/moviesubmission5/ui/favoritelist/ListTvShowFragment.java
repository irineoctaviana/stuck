package com.example.moviesubmission5.ui.favoritelist;


import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.moviesubmission5.R;
import com.example.moviesubmission5.adapter.WatchAdapterFavorite;
import com.example.moviesubmission5.callback.LoadWatchCallback;
import com.example.moviesubmission5.db.DatabaseContract;
import com.example.moviesubmission5.entity.Watch;
import com.example.moviesubmission5.helper.MappingHelper;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;

public class ListTvShowFragment extends Fragment implements LoadWatchCallback {
    private RecyclerView rvWatch;
    private ProgressBar progressBar;
    private static final String EXTRA_STATE = "EXTRA_STATE";
    WatchAdapterFavorite watchAdapter;
    LinearLayoutManager linearLayoutManager;
    DividerItemDecoration divItemDecor;
    private static HandlerThread handlerThread;
    private DataObserver myObserver;

    public ListTvShowFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_tv_show, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvWatch = view.findViewById(R.id.rv_tv_show_favorite);
        rvWatch.setHasFixedSize(true);
        progressBar = view.findViewById(R.id.progressBarListFavTvShow);
        showRecyclerList();

        if (getContext() == null){
            Log.d("irin", "ListTvShowFragment::onViewCreated::getContext() == null");
        } else {
            Log.d("irin", "ListTvShowFragment::onViewCreated::getContext() != null");
        }
        if (savedInstanceState == null){
            new LoadFavoriteAsync(getContext(), this).execute();
        } else{
            ArrayList<Watch> watchList = savedInstanceState.getParcelableArrayList(EXTRA_STATE);
            if(watchList != null){
                watchAdapter.setFavoriteWatch(watchList);
            }
        }
    }

    private void showRecyclerList(){ ;
        linearLayoutManager = new LinearLayoutManager(getContext());
        divItemDecor = new DividerItemDecoration(rvWatch.getContext(), linearLayoutManager.getOrientation());
        rvWatch.addItemDecoration(divItemDecor);
        rvWatch.setLayoutManager(linearLayoutManager);

        handlerThread = new HandlerThread("DataObserver");
        handlerThread.start();
        Handler handler = new Handler(handlerThread.getLooper());
        myObserver = new DataObserver(handler, getContext());
        getContext().getContentResolver().registerContentObserver(DatabaseContract.CONTENT_TV_SHOW_URI, true, myObserver);
        if (myObserver == null){
            Log.d("irin", "ListTvShowFragment::showRecyclerList::myObserver == null");
        } else {
            Log.d("irin", "ListTvShowFragment::showRecyclerList::myObserver != null");
        }

        watchAdapter = new WatchAdapterFavorite(getActivity());
        rvWatch.setAdapter(watchAdapter);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(EXTRA_STATE, watchAdapter.getListWatch());
    }

    @Override
    public void preExecute() {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable(){
            @Override
            public void run(){
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void postExecute(Cursor notes) {
        progressBar.setVisibility(View.INVISIBLE);
        if(notes == null){
            Log.d("irin", "ListTvShowFragment::postExecute::tvshows == null");
        } else {
            Log.d("irin", "ListTvShowFragment::postExecute::tvshows != null");
        }
        ArrayList<Watch> listTvShows = MappingHelper.mapCursorToArrayList(notes);
        if (listTvShows.size() > 0){
            watchAdapter.setFavoriteWatch(listTvShows);
        } else {
            watchAdapter.setFavoriteWatch(new ArrayList<Watch>());
            Toast.makeText(getActivity(), "ListTvShows Data is NULL", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadFavoriteAsync(getContext(), this).execute();
    }

    private static class LoadFavoriteAsync extends AsyncTask<Void, Void, Cursor> {
        private final WeakReference<Context> weakContext;
        private final WeakReference<LoadWatchCallback> weakCallback;

        private LoadFavoriteAsync(Context context, LoadWatchCallback callback) {
            weakContext = new WeakReference<>(context);
            weakCallback = new WeakReference<>(callback);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(weakCallback == null){
                Log.d("irin", "ListTvShowFragment::LoadFavoriteAsync::onPreExecute::weakCallback == null");
            } else {
                Log.d("irin", "ListTvShowFragment::LoadFavoriteAsync::onPreExecute::weakCallback != null");
            }
            weakCallback.get().preExecute();
        }

        @Override
        protected void onPostExecute(Cursor notes) {
            super.onPostExecute(notes);
            if(notes == null){
                Log.d("irin", "ListTvShowFragment::LoadFavoriteAsync::postExecute::tvshows == null");
            } else {
                Log.d("irin", "ListTvShowFragment::LoadFavoriteAsync::postExecute::tvshows != null");
            }
            weakCallback.get().postExecute(notes);
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            Context context = weakContext.get();
            if(context == null){
                Log.d("irin", "ListTvShowFragment::LoadFavoriteAsync::doInBackground::context == null");
            } else {
                Log.d("irin", "ListTvShowFragment::LoadFavoriteAsync::doInBackground::context != null");
            }
            return context.getContentResolver().query(DatabaseContract.CONTENT_TV_SHOW_URI, null, null, null, null);
        }
    }

    public static class DataObserver extends ContentObserver {
        final Context context;

        public DataObserver(Handler handler, Context context){
            super(handler);
            this.context = context;
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            new LoadFavoriteAsync(context, (LoadWatchCallback) context).execute();
        }
    }

}
