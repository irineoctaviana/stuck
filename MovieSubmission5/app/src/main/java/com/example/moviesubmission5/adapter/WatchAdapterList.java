package com.example.moviesubmission5.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.moviesubmission5.R;
import com.example.moviesubmission5.entity.Watch;

import java.util.ArrayList;

public class WatchAdapterList extends RecyclerView.Adapter<WatchAdapterList.WatchViewHolder> {
    private Context context;
    private ArrayList<Watch> listWatch = new ArrayList<>();

    public WatchAdapterList(Context context) { this.context = context; }

    public void setListWatch(ArrayList<Watch> watch) {
        if (watch.size()>0){
            this.listWatch.clear();
        }
        listWatch.addAll(watch);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WatchViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View itemRow = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_layout, viewGroup, false);
        return new WatchViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull WatchViewHolder watchViewHolder, int position) {
        watchViewHolder.bind(listWatch.get(position));
    }

    @Override
    public int getItemCount() {
        return listWatch.size();
    }

    public class WatchViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvTitle;
        TextView tvScore;
        TextView tvSynopsis;

        public WatchViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_photo);
            tvTitle = itemView.findViewById(R.id.txt_title);
            tvScore = itemView.findViewById(R.id.txt_score);
            tvSynopsis = itemView.findViewById(R.id.txt_overview);
        }

        public void bind(Watch watch) {
            Glide.with(context)
                    .asBitmap()
                    .load(watch.getPhotoList())
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                    .into(imgPhoto);
            tvTitle.setText(watch.getTitleList());
            tvScore.setText(watch.getScoreList());
            tvSynopsis.setText(watch.getSynopsisList());
        }
    }
}
