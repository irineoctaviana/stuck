package com.example.moviesubmission5.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.moviesubmission5.BuildConfig;
import com.example.moviesubmission5.entity.Watch;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class MainViewModel extends ViewModel {
    private String API_KEY = BuildConfig.TMDB_API_KEY;
    private String SIZE_PHOTO = "w185";
    private String[] listIdMovie;
    private String[] listIdTvShow;
    private String[] listPhotoMovieFavorite;
    private String[] listTitleMovieFavorite;
    private String[] listScoreMovieFavorite;
    private String[] listSynopsisMovieFavorite;
    private String[] listPhotoTvShowFavorite;
    private String[] listTitleTvShowFavorite;
    private String[] listScoreTvShowFavorite;
    private String[] listSynopsisTvShowFavorite;
    private String mLocale;

    private MutableLiveData<ArrayList<Watch>> listWatchMovie = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Watch>> listWatchTvShow = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Watch>> detailWatchMovie = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Watch>> detailWatchTvShow = new MutableLiveData<>();
    private MutableLiveData<String> errorMessageList =  new MutableLiveData<>();
    private MutableLiveData<String> errorMessageDetail =  new MutableLiveData<>();

    public LiveData<ArrayList<Watch>> getListWatchMovie() { return listWatchMovie; }
    public LiveData<ArrayList<Watch>> getListWatchTvShow() { return listWatchTvShow; }
    public LiveData<ArrayList<Watch>> getDetailWatchMovie() { return detailWatchMovie; }
    public LiveData<ArrayList<Watch>> getDetailWatchTvShow() { return detailWatchTvShow; }
    public LiveData<String> getErrorMessageList() { return errorMessageList; }
    public LiveData<String> getErrorMessageDetail() { return errorMessageDetail; }

    public void setListWatchMovie() {
        setLocale(Locale.getDefault().getDisplayLanguage());
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<Watch> listWatches = new ArrayList<>();
        String listUrl = "https://api.themoviedb.org/3/discover/movie?api_key="+API_KEY+"&language="+getLocale();

        client.get(listUrl, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try{
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");
                    listIdMovie = new String[list.length()];
                    listPhotoMovieFavorite = new String[list.length()];
                    listTitleMovieFavorite =  new String[list.length()];
                    listScoreMovieFavorite = new String[list.length()];
                    listSynopsisMovieFavorite = new String[list.length()];

                    for (int i=0; i<list.length(); i++) {
                        JSONObject watches = list.getJSONObject(i);
                        Watch watch = new Watch();
                        watch.watchListMovie(watches);
                        listWatches.add(watch);
                        String urlPhoto = "https://image.tmdb.org/t/p/"+SIZE_PHOTO+watch.getPhotoPathList();
                        watch.setPhotoList(urlPhoto);
                        listIdMovie[i] = watch.getId();
                        listPhotoMovieFavorite[i] = urlPhoto;
                        listTitleMovieFavorite[i] = watch.getTitleList();
                        listScoreMovieFavorite[i] = watch.getScoreList();
                        listSynopsisMovieFavorite[i] = watch.getSynopsisList();
                    }
                    listWatchMovie.postValue(listWatches);
                } catch(Exception e){
                    listWatchMovie.postValue(null);
                    setErrorMessageList(e.getMessage());
                    Log.d("Exception", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                listWatchMovie.postValue(null);
                setErrorMessageList(error.getMessage());
                Log.d("onFailure", error.getMessage());
            }
        });
    }

    public void setListWatchTvShow() {
        setLocale(Locale.getDefault().getDisplayLanguage());
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<Watch> listWatches = new ArrayList<>();
        String listUrl = "https://api.themoviedb.org/3/discover/tv?api_key="+API_KEY+"&language="+getLocale();

        client.get(listUrl, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try{
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");
                    listIdTvShow = new String[list.length()];
                    listPhotoTvShowFavorite = new String[list.length()];
                    listTitleTvShowFavorite =  new String[list.length()];
                    listScoreTvShowFavorite = new String[list.length()];
                    listSynopsisTvShowFavorite = new String[list.length()];

                    for (int i=0; i<list.length(); i++) {
                        JSONObject watches = list.getJSONObject(i);
                        Watch watch = new Watch();
                        watch.watchListTVShow(watches);
                        listWatches.add(watch);
                        String urlPhoto = "https://image.tmdb.org/t/p/"+SIZE_PHOTO+watch.getPhotoPathList();
                        watch.setPhotoList(urlPhoto);
                        listIdTvShow[i] = watch.getId();
                        listPhotoTvShowFavorite[i] = urlPhoto;
                        listTitleTvShowFavorite[i] = watch.getTitleList();
                        listScoreTvShowFavorite[i] = watch.getScoreList();
                        listSynopsisTvShowFavorite[i] = watch.getSynopsisList();
                    }
                    listWatchTvShow.postValue(listWatches);
                } catch(Exception e){
                    listWatchTvShow.postValue(null);
                    setErrorMessageList(e.getMessage());
                    Log.d("Exception", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                listWatchTvShow.postValue(null);
                setErrorMessageList(error.getMessage());
                Log.d("onFailure", error.getMessage());
            }
        });
    }

    public void setDetailWatchMovie(final String idWatch) {
        setLocale(Locale.getDefault().getDisplayLanguage());
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<Watch> detailWatches = new ArrayList<>();
        String url = "https://api.themoviedb.org/3/movie/"+idWatch+"?api_key="+API_KEY+"&language="+getLocale();

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try{
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    Watch watch = new Watch();
                    watch.watchDetailMovie(responseObject);
                    detailWatches.add(watch);
                    detailWatchMovie.postValue(detailWatches);
                    String urlPhoto = "https://image.tmdb.org/t/p/"+SIZE_PHOTO+watch.getPhotoPathDetail();
                    watch.setPhotoDetail(urlPhoto);
                } catch (Exception e){
                    detailWatchMovie.postValue(null);
                    setErrorMessageDetail(e.getMessage());
                    Log.d("Exception", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                detailWatchMovie.postValue(null);
                setErrorMessageDetail(error.getMessage());
                Log.d("onFailure", error.getMessage());
            }
        });
    }

    public void setDetailWatchTvShow(final String idWatch) {
        setLocale(Locale.getDefault().getDisplayLanguage());
        AsyncHttpClient client = new AsyncHttpClient();
        final ArrayList<Watch> detailWatches = new ArrayList<>();
        String url = "https://api.themoviedb.org/3/tv/"+idWatch+"?api_key="+API_KEY+"&language="+getLocale();

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try{
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    Watch watch = new Watch();
                    watch.watchDetailTvShow(responseObject);
                    detailWatches.add(watch);
                    detailWatchTvShow.postValue(detailWatches);
                    String urlPhoto = "https://image.tmdb.org/t/p/"+SIZE_PHOTO+watch.getPhotoPathDetail();
                    watch.setPhotoDetail(urlPhoto);
                } catch (Exception e){
                    detailWatchTvShow.postValue(null);
                    setErrorMessageDetail(e.getMessage());
                    Log.d("Exception", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                detailWatchTvShow.postValue(null);
                setErrorMessageDetail(error.getMessage());
                Log.d("onFailure", error.getMessage());
            }
        });
    }

    private void setErrorMessageList(final String message) { errorMessageList.postValue(message); }

    private void setErrorMessageDetail(final String message) { errorMessageDetail.postValue(message); }

    public String getListIdMovie (int position){ return listIdMovie[position]; }

    public String getListIdTvShow (int position){ return listIdTvShow[position]; }

    public String getListPhotoMovieFavorite (int position){ return listPhotoMovieFavorite[position]; }

    public String getListTitleMovieFavorite (int position){ return listTitleMovieFavorite[position]; }

    public String getListScoreMovieFavorite (int position){ return listScoreMovieFavorite[position]; }

    public String getListSynopsisMovieFavorite (int position){ return listSynopsisMovieFavorite[position]; }

    public String getListPhotoTvShowFavorite (int position){ return listPhotoTvShowFavorite[position]; }

    public String getListTitleTvShowFavorite (int position){ return listTitleTvShowFavorite[position]; }

    public String getListScoreTvShowFavorite (int position){ return listScoreTvShowFavorite[position]; }

    public String getListSynopsisTvShowFavorite (int position){ return listSynopsisTvShowFavorite[position]; }

    private String getLocale() { return this.mLocale; }

    private void setLocale(String locale) {
        if (locale.equals("English")) { this.mLocale = "en-US"; }
        else { this.mLocale = "id-ID"; }
    }
}
