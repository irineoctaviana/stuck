package com.example.moviesubmission5.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.moviesubmission5.db.DatabaseContract.WatchColumns;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static String DATABASE_NAME = "dbsubmission";
    private static final int DATABASE_VERSION = 1;
    private static final String SQL_CREATE_TABLE_MOVIE = String.format("CREATE TABLE %s"+
            " (%s INTEGER PRIMARY KEY AUTOINCREMENT, "+
            " %s INTEGER NOT NULL," +
            " %s TEXT NOT NULL," +
            " %s TEXT NOT NULL," +
            " %s TEXT NOT NULL," +
            " %s TEXT NOT NULL," +
            " %s TEXT NOT NULL)",
            DatabaseContract.TABLE_MOVIE,
            WatchColumns._ID,
            WatchColumns.WATCH_ID,
            WatchColumns.WATCH_IS_MOVIE,
            WatchColumns.WATCH_PHOTO,
            WatchColumns.WATCH_TITLE,
            WatchColumns.WATCH_SCORE,
            WatchColumns.WATCH_SYNOPSIS);

    private static final String SQL_CREATE_TABLE_TV_SHOW = String.format("CREATE TABLE %s"+
            " (%s INTEGER PRIMARY KEY AUTOINCREMENT, "+
            " %s INTEGER NOT NULL," +
            " %s TEXT NOT NULL," +
            " %s TEXT NOT NULL," +
            " %s TEXT NOT NULL," +
            " %s TEXT NOT NULL," +
            " %s TEXT NOT NULL)",
            DatabaseContract.TABLE_TV_SHOW,
            WatchColumns._ID,
            WatchColumns.WATCH_ID,
            WatchColumns.WATCH_IS_MOVIE,
            WatchColumns.WATCH_PHOTO,
            WatchColumns.WATCH_TITLE,
            WatchColumns.WATCH_SCORE,
            WatchColumns.WATCH_SYNOPSIS);

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_MOVIE);
        db.execSQL(SQL_CREATE_TABLE_TV_SHOW);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.TABLE_MOVIE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.TABLE_TV_SHOW);
        onCreate(db);
    }
}
