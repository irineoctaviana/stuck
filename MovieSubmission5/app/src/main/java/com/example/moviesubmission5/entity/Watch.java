package com.example.moviesubmission5.entity;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.moviesubmission5.db.DatabaseContract;
import com.example.moviesubmission5.db.DatabaseContract.WatchColumns;

import org.json.JSONArray;
import org.json.JSONObject;

public class Watch implements Parcelable {
    private String id;
    private String photoList;
    private String photoPathList;
    private String titleList;
    private String scoreList;
    private String synopsisList;
    private String photoDetail;
    private String photoPathDetail;
    private String titleDetail;
    private String scoreDetail;
    private String synopsisDetail;
    private String release;
    private String company;
    private String genre;
    private String photoFavorite;
    private String titleFavorite;
    private String scoreFavorite;
    private String synopsisFavorite;
    private String isMovie;

    public String getId() { return id; }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhotoList() {
        return photoList;
    }

    public void setPhotoList(String photo) { this.photoList = photo; }

    public String getPhotoPathList() { return photoPathList; }

    public String getTitleList() {
        return titleList;
    }

    public String getScoreList() {
        return scoreList;
    }

    public String getSynopsisList() {
        return synopsisList;
    }

    public String getPhotoDetail() { return photoDetail; }

    public void setPhotoDetail(String photo) { this.photoDetail = photo; }

    public String getPhotoPathDetail() { return photoPathDetail; }

    public String getTitleDetail() { return titleDetail; }

    public String getScoreDetail() {
        return scoreDetail;
    }

    public String getSynopsisDetail() {
        return synopsisDetail;
    }

    public String getRelease() {
        return release;
    }

    public String getCompany() { return company; }

    public String getGenre() {
        return genre;
    }

    public String getIsMovie() { return isMovie; }

    public void setIsMovie(String movie) { isMovie = movie; }

    public String getPhotoFavorite() {
        return photoFavorite;
    }

    public void setPhotoFavorite(String photoFavorite) {
        this.photoFavorite = photoFavorite;
    }

    public String getTitleFavorite() { return titleFavorite; }

    public void setTitleFavorite(String titleFavorite) { this.titleFavorite = titleFavorite; }

    public String getScoreFavorite() {
        return scoreFavorite;
    }

    public void setScoreFavorite(String scoreFavorite) {
        this.scoreFavorite = scoreFavorite;
    }

    public String getSynopsisFavorite() {
        return synopsisFavorite;
    }

    public void setSynopsisFavorite(String synopsisFavorite) { this.synopsisFavorite = synopsisFavorite; }

    public void watchListMovie(JSONObject object){
        try{
            String id = object.getString("id");
            String photoPath = object.getString("poster_path");
            String title = object.getString("title");
            String score = object.getString("vote_average");
            String synopsis = object.getString("overview");
            this.id = id;
            this.photoPathList = photoPath;
            this.titleList = title;
            this.scoreList = score;
            this.synopsisList = synopsis;
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void watchListTVShow(JSONObject object){
        try{
            String id = object.getString("id");
            String photoPath = object.getString("poster_path");
            String title = object.getString("name");
            String score = object.getString("vote_average");
            String synopsis = object.getString("overview");
            this.id = id;
            this.photoPathList = photoPath;
            this.titleList = title;
            this.scoreList = score;
            this.synopsisList = synopsis;
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void watchDetailMovie(JSONObject object){
        try {
            String title = object.getString("original_title");
            String photoPath = object.getString("poster_path");
            String score = object.getString("vote_average");
            String release = object.getString("release_date");
            String synopsis = object.getString("overview");

            JSONArray listCompany = object.getJSONArray("production_companies");
            StringBuilder company = new StringBuilder();
            try{
                if (listCompany.length() > 0){
                    company.append(object.getJSONArray("production_companies").getJSONObject(0).getString("name"));
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            JSONArray listGenre = object.getJSONArray("genres");
            StringBuilder genre = new StringBuilder();
            try{
                int i=0;
                while (i<listGenre.length()){
                    genre.append(object.getJSONArray("genres").getJSONObject(i).getString("name"));
                    i++;
                    if (i != listGenre.length()){
                        genre.append(", ");
                    }
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            this.photoPathDetail = photoPath;
            this.titleDetail = title;
            this.scoreDetail = score;
            this.synopsisDetail = synopsis;
            this.release = release;
            this.company = company.toString();
            this.genre = genre.toString();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public void watchDetailTvShow(JSONObject object){
        try {
            String title = object.getString("original_name");
            String photoPath = object.getString("poster_path");
            String score = object.getString("vote_average");
            String release = object.getString("first_air_date");
            String synopsis = object.getString("overview");

            JSONArray listCompany = object.getJSONArray("production_companies");
            StringBuilder company = new StringBuilder();
            try{
                if (listCompany.length() > 0){
                    company.append(object.getJSONArray("production_companies").getJSONObject(0).getString("name"));
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            JSONArray listGenre = object.getJSONArray("genres");
            StringBuilder genre = new StringBuilder();
            try{
                int i=0;
                while (i<listGenre.length()){
                    genre.append(object.getJSONArray("genres").getJSONObject(i).getString("name"));
                    i++;
                    if (i != listGenre.length()){
                        genre.append(", ");
                    }
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            this.photoPathDetail = photoPath;
            this.titleDetail = title;
            this.scoreDetail = score;
            this.synopsisDetail = synopsis;
            this.release = release;
            this.company = company.toString();
            this.genre = genre.toString();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.isMovie);
        dest.writeString(this.photoFavorite);
        dest.writeString(this.titleFavorite);
        dest.writeString(this.scoreFavorite);
        dest.writeString(this.synopsisFavorite);
    }

    public Watch() { }

    public Watch(String id, String isMovie, String photoFavorite, String titleFavorite, String scoreFavorite, String synopsisFavorite){
        this.id = id;
        this.isMovie = isMovie;
        this.photoFavorite = photoFavorite;
        this.titleFavorite = titleFavorite;
        this.scoreFavorite = scoreFavorite;
        this.synopsisFavorite = synopsisFavorite;
    }

    public Watch(Cursor cursor){
        this.id = DatabaseContract.getColumnString(cursor, WatchColumns.WATCH_ID);
        this.isMovie = DatabaseContract.getColumnString(cursor, WatchColumns.WATCH_IS_MOVIE);
        this.photoFavorite = DatabaseContract.getColumnString(cursor, WatchColumns.WATCH_PHOTO);
        this.titleFavorite = DatabaseContract.getColumnString(cursor, WatchColumns.WATCH_TITLE);
        this.scoreFavorite = DatabaseContract.getColumnString(cursor, WatchColumns.WATCH_SCORE);
        this.synopsisFavorite = DatabaseContract.getColumnString(cursor, WatchColumns.WATCH_SYNOPSIS);
    }

    protected Watch(Parcel in) {
        this.id = in.readString();
        this.isMovie = in.readString();
        this.photoFavorite = in.readString();
        this.titleFavorite = in.readString();
        this.scoreFavorite = in.readString();
        this.synopsisFavorite = in.readString();
    }

    public static final Parcelable.Creator<Watch> CREATOR = new Parcelable.Creator<Watch>() {
        @Override
        public Watch createFromParcel(Parcel source) {
            return new Watch(source);
        }

        @Override
        public Watch[] newArray(int size) {
            return new Watch[size];
        }
    };
}
