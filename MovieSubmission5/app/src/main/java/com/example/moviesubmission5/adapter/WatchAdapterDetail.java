package com.example.moviesubmission5.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.moviesubmission5.R;
import com.example.moviesubmission5.activity.DetailActivity;
import com.example.moviesubmission5.entity.Watch;

import java.util.ArrayList;

public class WatchAdapterDetail extends RecyclerView.Adapter<WatchAdapterDetail.WatchDetailViewHolder>{
    private Context context;
    private ArrayList<Watch> detailWatchAdapter = new ArrayList<>();

    public WatchAdapterDetail(Context context)
    {
        this.context = context;
    }

    public void setDetailWatchAdapter(ArrayList<Watch> watch) {
        if (watch.size()>0){
            this.detailWatchAdapter.clear();
        }
        detailWatchAdapter.addAll(watch);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WatchDetailViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_detail, viewGroup, false);
        return new WatchDetailViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull WatchDetailViewHolder watchViewHolder, int position) {
        watchViewHolder.bind(detailWatchAdapter.get(position));
    }

    @Override
    public int getItemCount() {
        return detailWatchAdapter.size();
    }

    public class WatchDetailViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvTitle;
        TextView tvScore;
        TextView tvRelease;
        TextView tvCompany;
        TextView tvGenre;
        TextView tvSynopsis;

        public WatchDetailViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.detail_photo);
            tvTitle = itemView.findViewById(R.id.detail_title);
            tvScore = itemView.findViewById(R.id.detail_score);
            tvRelease = itemView.findViewById(R.id.detail_release );
            tvCompany = itemView.findViewById(R.id.detail_company );
            tvGenre = itemView.findViewById(R.id.detail_genre );
            tvSynopsis = itemView.findViewById(R.id.detail_synopsis);
        }

        public void bind(Watch watch) {
            Glide.with(context)
                    .asBitmap()
                    .load(watch.getPhotoDetail())
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                    .into(imgPhoto);
            tvTitle.setText(watch.getTitleDetail());
            tvScore.setText(watch.getScoreDetail());
            tvRelease.setText(watch.getRelease());
            tvCompany.setText(watch.getCompany());
            tvGenre.setText(watch.getGenre());
            tvSynopsis.setText(watch.getSynopsisDetail());

            Intent detailIntent = new Intent();
            watch.setPhotoFavorite(watch.getPhotoDetail());
            watch.setTitleFavorite(watch.getTitleDetail());
            watch.setScoreFavorite(watch.getScoreDetail());
            watch.setSynopsisFavorite(watch.getSynopsisDetail());
            detailIntent.putExtra(DetailActivity.EXTRA_WATCH, watch);

        }
    }
}
