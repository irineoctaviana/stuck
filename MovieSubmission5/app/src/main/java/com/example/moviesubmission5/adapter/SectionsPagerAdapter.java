package com.example.moviesubmission5.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.moviesubmission5.R;
import com.example.moviesubmission5.ui.favoritelist.ListTvShowFragment;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_movie, R.string.tab_tv_show};
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                //return new ListMovieFragment();
                return new ListTvShowFragment();
            case 1:
                return new ListTvShowFragment();
                //return new ListMovieFragment();
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return 2;
    }
}
