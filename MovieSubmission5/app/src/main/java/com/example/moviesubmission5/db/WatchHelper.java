package com.example.moviesubmission5.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.moviesubmission5.db.DatabaseContract.WatchColumns;
import com.example.moviesubmission5.entity.Watch;

import java.util.ArrayList;

import static com.example.moviesubmission5.db.DatabaseContract.TABLE_MOVIE;
import static com.example.moviesubmission5.db.DatabaseContract.TABLE_TV_SHOW;

public class WatchHelper {
    private static final String DATABASE_TABLE_MOVIE = TABLE_MOVIE;
    private static final String DATABASE_TABLE_TV_SHOW = TABLE_TV_SHOW;
    private static DatabaseHelper dbHelper;
    private static WatchHelper INSTANCE;
    private static SQLiteDatabase db;

    public WatchHelper(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public static WatchHelper getInstance(Context context){
        if (INSTANCE == null){
            synchronized (SQLiteOpenHelper.class){
                if (INSTANCE == null){
                    INSTANCE = new WatchHelper(context);
                }
            }
        }
        return INSTANCE;
    }

    public void open() throws SQLException{
        db = dbHelper.getWritableDatabase();
    }

    public void close(){
        dbHelper.close();
        if (db.isOpen()) {
            db.close();
        }
    }

    public ArrayList<Watch> getAllWatchMovie(){
        ArrayList<Watch> arrayList = new ArrayList<>();
        Cursor cursor = db.query(DATABASE_TABLE_MOVIE,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 WatchColumns.WATCH_ID + " ASC");
        if(cursor != null) {
            cursor.moveToFirst();
            Watch watch;
            if (cursor.getCount() > 0) {
                do {
                    watch = new Watch();
                    watch.setId(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_ID)));
                    watch.setIsMovie(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_IS_MOVIE)));
                    watch.setPhotoFavorite(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_PHOTO)));
                    watch.setTitleFavorite(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_TITLE)));
                    watch.setScoreFavorite(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_SCORE)));
                    watch.setSynopsisFavorite(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_SYNOPSIS)));

                    arrayList.add(watch);
                    cursor.moveToNext();
                } while (!cursor.isAfterLast());
            }
            cursor.close();
        }
        return arrayList;
    }

    public ArrayList<Watch> getAllWatchTvShow(){
        ArrayList<Watch> arrayList = new ArrayList<>();
        Cursor cursor = db.query(DATABASE_TABLE_TV_SHOW,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 WatchColumns.WATCH_ID + " ASC");
        if(cursor != null) {
            cursor.moveToFirst();
            Watch watch;
            if (cursor.getCount() > 0) {
                do {
                    watch = new Watch();
                    watch.setId(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_ID)));
                    watch.setIsMovie(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_IS_MOVIE)));
                    watch.setPhotoFavorite(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_PHOTO)));
                    watch.setTitleFavorite(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_TITLE)));
                    watch.setScoreFavorite(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_SCORE)));
                    watch.setSynopsisFavorite(cursor.getString(cursor.getColumnIndexOrThrow(WatchColumns.WATCH_SYNOPSIS)));

                    arrayList.add(watch);
                    cursor.moveToNext();
                } while (!cursor.isAfterLast());
            }
            cursor.close();
        }
        return arrayList;
    }

    public boolean checkRowMovie(String id){
        String[] columns = {WatchColumns.WATCH_ID};
        String selection = WatchColumns.WATCH_ID + " =?";
        String[] selectionArgs = {id};
        String limit = "1";
        Cursor cursor = db.query(DATABASE_TABLE_MOVIE,
                                 columns,
                                 selection,
                                 selectionArgs,
                                 null,
                                 null,
                                 limit);
        if(cursor != null) {
            cursor.moveToFirst();
            if (cursor.getCount()>0) {
                cursor.close();
                return false;
            }
            else{
                cursor.close();
                return true;
            }
        }
        return false;
    }

    public boolean checkRowTvShow(String id){
        String[] columns = {WatchColumns.WATCH_ID};
        String selection = WatchColumns.WATCH_ID + " =?";
        String[] selectionArgs = {id};
        String limit = "1";
        Cursor cursor = db.query(DATABASE_TABLE_TV_SHOW,
                                 columns,
                                 selection,
                                 selectionArgs,
                                 null,
                                 null,
                                 limit);
        if(cursor != null) {
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                cursor.close();
                return false;
            } else {
                cursor.close();
                return true;
            }
        }
        return false;
    }

    public long insertMovie(Watch watch){
        ContentValues args = new ContentValues();
        args.put(WatchColumns.WATCH_ID, watch.getId());
        args.put(WatchColumns.WATCH_IS_MOVIE, watch.getIsMovie());
        args.put(WatchColumns.WATCH_PHOTO, watch.getPhotoFavorite());
        args.put(WatchColumns.WATCH_TITLE, watch.getTitleFavorite());
        args.put(WatchColumns.WATCH_SCORE, watch.getScoreFavorite());
        args.put(WatchColumns.WATCH_SYNOPSIS, watch.getSynopsisFavorite());
        return db.insert(DATABASE_TABLE_MOVIE, null, args);
    }

    public long insertTvShow(Watch watch){
        ContentValues args = new ContentValues();
        args.put(WatchColumns.WATCH_ID, watch.getId());
        args.put(WatchColumns.WATCH_IS_MOVIE, watch.getIsMovie());
        args.put(WatchColumns.WATCH_PHOTO, watch.getPhotoFavorite());
        args.put(WatchColumns.WATCH_TITLE, watch.getTitleFavorite());
        args.put(WatchColumns.WATCH_SCORE, watch.getScoreFavorite());
        args.put(WatchColumns.WATCH_SYNOPSIS, watch.getSynopsisFavorite());
        return db.insert(DATABASE_TABLE_TV_SHOW, null, args);
    }

    public int deleteMovie(String id){
        return db.delete(TABLE_MOVIE, WatchColumns.WATCH_ID + " = '"+id+"'", null);
    }

    public int deleteTvShow(String id){
        return db.delete(TABLE_TV_SHOW, WatchColumns.WATCH_ID + " = '"+id+"'", null);
    }

    public Cursor queryByIdMovieProvider(String id){
        return db.query(DATABASE_TABLE_MOVIE,
                                 null,
                                 WatchColumns.WATCH_ID + " =?",
                                 new String[]{id},
                                 null,
                                 null,
                                 null,
                                 null);
    }

    public Cursor queryByIdTvShowProvider(String id){
        return db.query(DATABASE_TABLE_TV_SHOW,
                        null,
                        WatchColumns.WATCH_ID + " =?",
                        new String[]{id},
                        null,
                        null,
                        null,
                        null);
    }

    public Cursor queryMovieProvider(){
        return db.query(DATABASE_TABLE_MOVIE,
                        null,
                        null,
                        null,
                        null,
                        null,
                        WatchColumns.WATCH_ID + " ASC");
    }

    public Cursor queryTvShowProvider(){
        return db.query(DATABASE_TABLE_TV_SHOW,
                        null,
                        null,
                        null,
                        null,
                        null,
                        WatchColumns.WATCH_ID + " ASC");
    }

    public long insertMovieProvider(ContentValues values){
        return db.insert(DATABASE_TABLE_MOVIE, null, values);
    }

    public long insertTvShowProvider(ContentValues values){
        return db.insert(DATABASE_TABLE_TV_SHOW, null, values);
    }

    public int updateMovieProvider(String id, ContentValues values){
        return db.update(DATABASE_TABLE_MOVIE, values, WatchColumns.WATCH_ID + " =?", new String[]{id});
    }

    public int updateTvShowProvider(String id, ContentValues values){
        return db.update(DATABASE_TABLE_TV_SHOW, values, WatchColumns.WATCH_ID + " =?", new String[]{id});
    }

    public int deleteMovieProvider(String id){
        return db.delete(DATABASE_TABLE_MOVIE, WatchColumns.WATCH_ID + " =?", new String[]{id});
    }

    public int deleteTvShowProvider(String id){
        return db.delete(DATABASE_TABLE_TV_SHOW, WatchColumns.WATCH_ID + " =?", new String[]{id});
    }
}
