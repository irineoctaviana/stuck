package com.example.moviesubmission5.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.moviesubmission5.R;
import com.example.moviesubmission5.activity.DetailActivity;
import com.example.moviesubmission5.db.DatabaseContract;
import com.example.moviesubmission5.entity.Watch;
import com.example.moviesubmission5.listener.CustomOnItemClickListener;

import java.util.ArrayList;

public class WatchAdapterFavorite extends RecyclerView.Adapter<WatchAdapterFavorite.WatchFavoriteViewHolder> {
    private Activity activity;
    private ArrayList<Watch> favoriteWatch = new ArrayList<>();

    public WatchAdapterFavorite(Activity activity) { this.activity = activity; }

    public ArrayList<Watch> getListWatch() { return favoriteWatch; }

    public void setFavoriteWatch(ArrayList<Watch> watch) {
        this.favoriteWatch.clear();
        this.favoriteWatch.addAll(watch);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WatchFavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View itemRow = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_layout, viewGroup, false);
        return new WatchFavoriteViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull WatchFavoriteViewHolder watchViewHolder, int position) {
        watchViewHolder.bind(getListWatch().get(position));

        watchViewHolder.itemView.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                Intent watchIntent = new Intent(activity, DetailActivity.class);
                Uri uri;
                Log.d("irin", "WatchAdapterFavorite::onBindViewHolder::getIsMovie() " +getListWatch().get(position).getIsMovie().equals("1"));
                if (getListWatch().get(position).getIsMovie().equals("1")){
                    uri = Uri.parse(DatabaseContract.CONTENT_MOVIE_URI +"/"+ getListWatch().get(position).getId());
                } else {
                    uri = Uri.parse(DatabaseContract.CONTENT_TV_SHOW_URI +"/"+ getListWatch().get(position).getId());
                }
                if (uri == null){
                    Log.d("irin", "WatchAdapterFavorite::onBindViewHolder::uri is null ");
                } else{
                    Log.d("irin", "WatchAdapterFavorite::onBindViewHolder::uri is NOT null ");
                }
                watchIntent.setData(uri);
                Watch watch = new Watch();
                watch.setId(getListWatch().get(position).getId());
                watch.setIsMovie(getListWatch().get(position).getIsMovie());
                watch.setPhotoFavorite(getListWatch().get(position).getPhotoFavorite());
                watch.setTitleFavorite(getListWatch().get(position).getTitleFavorite());
                watch.setScoreFavorite(getListWatch().get(position).getScoreFavorite());
                watch.setSynopsisFavorite(getListWatch().get(position).getSynopsisFavorite());
                watchIntent.putExtra(DetailActivity.EXTRA_WATCH, watch);
                activity.startActivity(watchIntent);
            }
        }));
    }

    @Override
    public int getItemCount() {
        return favoriteWatch.size();
    }

    public class WatchFavoriteViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvTitle;
        TextView tvScore;
        TextView tvSynopsis;

        public WatchFavoriteViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_photo);
            tvTitle = itemView.findViewById(R.id.txt_title);
            tvScore = itemView.findViewById(R.id.txt_score);
            tvSynopsis = itemView.findViewById(R.id.txt_overview);
        }

        public void bind(Watch watch) {
            Glide.with(activity)
                    .asBitmap()
                    .load(watch.getPhotoFavorite())
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                    .into(imgPhoto);
            tvTitle.setText(watch.getTitleFavorite());
            tvScore.setText(watch.getScoreFavorite());
            tvSynopsis.setText(watch.getSynopsisFavorite());
        }
    }
}
