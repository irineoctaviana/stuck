package com.example.moviesubmission5.ui.movie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.support.annotation.Nullable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.widget.Toast;

import com.example.moviesubmission5.listener.ItemClickList;
import com.example.moviesubmission5.viewmodel.MainViewModel;
import com.example.moviesubmission5.R;
import com.example.moviesubmission5.entity.Watch;
import com.example.moviesubmission5.adapter.WatchAdapterList;
import com.example.moviesubmission5.activity.DetailActivity;

import java.util.ArrayList;

public class MovieFragment extends Fragment {
    private RecyclerView rvWatch;
    private WatchAdapterList watchAdapterList;
    private ProgressBar progressBarList;
    private MainViewModel mainViewModel;
    LinearLayoutManager linearLayoutManager;
    DividerItemDecoration divItemDecor;

    public MovieFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie, container, false);
    }

    @Override
    public void onViewCreated (@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mainViewModel.getListWatchMovie().observe(this, getListWatch);
        mainViewModel.getErrorMessageList().observe(this, getErrorMessageList);

        rvWatch = view.findViewById(R.id.rv_watch_movie);
        progressBarList = view.findViewById(R.id.progressBarListMovie);
        rvWatch.setHasFixedSize(true);
        showRecyclerList();
    }

    private void showRecyclerList(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        divItemDecor = new DividerItemDecoration(rvWatch.getContext(), linearLayoutManager.getOrientation());
        rvWatch.addItemDecoration(divItemDecor);
        rvWatch.setLayoutManager(linearLayoutManager);

        watchAdapterList = new WatchAdapterList(getContext());
        watchAdapterList.notifyDataSetChanged();
        rvWatch.setAdapter(watchAdapterList);
        mainViewModel.setListWatchMovie();
        showLoadingList(true);
        ItemClickList.addTo(rvWatch).setOnItemClickListener(new ItemClickList.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v){
                Intent watchIntent = new Intent(getActivity(), DetailActivity.class);
                Watch watch = new Watch();
                watch.setId(mainViewModel.getListIdMovie(position));
                watch.setIsMovie("1");
                watch.setPhotoFavorite(mainViewModel.getListPhotoMovieFavorite(position));
                watch.setTitleFavorite(mainViewModel.getListTitleMovieFavorite(position));
                watch.setScoreFavorite(mainViewModel.getListScoreMovieFavorite(position));
                watch.setSynopsisFavorite(mainViewModel.getListSynopsisMovieFavorite(position));
                watchIntent.putExtra(DetailActivity.EXTRA_WATCH, watch);
                startActivity(watchIntent);
            }
        });
    }

    private Observer<ArrayList<Watch>> getListWatch = new Observer<ArrayList<Watch>>() {
        @Override
        public void onChanged(ArrayList<Watch> watches) {
            if (watches != null){
                watchAdapterList.setListWatch(watches);
                showLoadingList(false);
            } else{
                showLoadingList(false);
            }
        }
    };

    private Observer<String> getErrorMessageList = new Observer<String>() {
        @Override
        public void onChanged(String messages) {
            Toast.makeText(getActivity(), messages, Toast.LENGTH_SHORT).show();
        }
    };

    private void showLoadingList(Boolean state){
        if (progressBarList != null){
            if (state){
                progressBarList.setVisibility(View.VISIBLE);
            } else {
                progressBarList.setVisibility(View.GONE);
            }
        }
    }
}