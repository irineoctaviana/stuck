package com.example.moviesubmission5.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.moviesubmission5.db.DatabaseContract;
import com.example.moviesubmission5.db.WatchHelper;
import com.example.moviesubmission5.ui.favoritelist.ListTvShowFragment;

public class WatchTvShowProvider extends ContentProvider {
    private static final int WATCH = 1;
    private static final int WATCH_ID = 2;
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private WatchHelper watchHelper;

    static{
        sUriMatcher.addURI(DatabaseContract.AUTHORITY, DatabaseContract.TABLE_TV_SHOW, WATCH);
        sUriMatcher.addURI(DatabaseContract.AUTHORITY, DatabaseContract.TABLE_TV_SHOW +"/#", WATCH_ID);
    }

    @Override
    public boolean onCreate() {
        watchHelper = WatchHelper.getInstance(getContext());
        if(watchHelper == null){
            Log.d("irin", "WatchTvShowProvider::onCreate::watchHelper == null");
        } else {
            Log.d("irin", "WatchTvShowProvider::onCreate::watchHelper != null");
        }
        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        watchHelper.open();
        if(watchHelper == null){
            Log.d("irin", "WatchTvShowProvider::query::watchHelper == null");
        } else {
            Log.d("irin", "WatchTvShowProvider::query::watchHelper != null");
        }
        Cursor cursorTvShow;
        switch (sUriMatcher.match(uri)){
            case WATCH:
                cursorTvShow = watchHelper.queryTvShowProvider();
                if(cursorTvShow == null){
                    Log.d("irin", "WatchTvShowProvider::query::WATCH::cursorTvShow == null");
                } else {
                    Log.d("irin", "WatchTvShowProvider::query::WATCH::cursorTvShow != null");
                }
                break;
            case WATCH_ID:
                cursorTvShow = watchHelper.queryByIdTvShowProvider(uri.getLastPathSegment());
                if(cursorTvShow == null){
                    Log.d("irin", "WatchTvShowProvider::query::WATCH_ID::cursorTvShow == null");
                } else {
                    Log.d("irin", "WatchTvShowProvider::query::WATCH_ID::cursorTvShow != null");
                }
                break;
            default:
                cursorTvShow = null;
                break;
        }
        return cursorTvShow;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
        watchHelper.open();
        long added;
        switch (sUriMatcher.match(uri)){
            case WATCH:
                added = watchHelper.insertTvShowProvider(contentValues);
                break;
            default:
                added = 0;
                break;
        }
        getContext().getContentResolver().notifyChange(DatabaseContract.CONTENT_TV_SHOW_URI, new ListTvShowFragment.DataObserver(new Handler(), getContext()));
        return Uri.parse(DatabaseContract.CONTENT_TV_SHOW_URI + "/" +added);
    }

    @Override
    public int delete(@NonNull Uri uri, String s, String[] strings) {
        watchHelper.open();
        int deleted;
        switch (sUriMatcher.match(uri)){
            case WATCH_ID:
                deleted = watchHelper.deleteTvShowProvider(uri.getLastPathSegment());
                break;
            default:
                deleted = 0;
                break;
        }
        getContext().getContentResolver().notifyChange(DatabaseContract.CONTENT_TV_SHOW_URI, new ListTvShowFragment.DataObserver(new Handler(), getContext()));
        return deleted;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues contentValues, String s, String[] strings) {
        watchHelper.open();
        int updated;
        switch (sUriMatcher.match(uri)){
            case WATCH_ID:
                updated = watchHelper.updateTvShowProvider(uri.getLastPathSegment(), contentValues);
                break;
            default:
                updated = 0;
                break;
        }
        getContext().getContentResolver().notifyChange(DatabaseContract.CONTENT_TV_SHOW_URI, new ListTvShowFragment.DataObserver(new Handler(), getContext()));
        return updated;
    }
}
