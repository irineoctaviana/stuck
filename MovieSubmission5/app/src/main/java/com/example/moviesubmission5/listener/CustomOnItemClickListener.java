package com.example.moviesubmission5.listener;

import android.view.View;

public class CustomOnItemClickListener implements View.OnClickListener{
    private int position;
    private OnItemClickCallback onItemCallback;

    public CustomOnItemClickListener(int position, OnItemClickCallback onItemCallback){
        this.position = position;
        this.onItemCallback = onItemCallback;
    }

    @Override
    public void onClick(View view){
        onItemCallback.onItemClicked(view, position);
    }

    public interface OnItemClickCallback{
        void onItemClicked(View view, int position);
    }
}
